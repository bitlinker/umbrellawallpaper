package info.no_ip.bitlinker.umbrellawallpaper;

import android.util.Log;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;

import timber.log.Timber;

/**
 * Main wallpaper class
 *
 * Created by Bitlinker on 12/5/2017.
 */

public class GdxWallpaper extends AndroidLiveWallpaperService implements AndroidWallpaperListener {
    @Override
    public void onCreateApplication() {
        super.onCreateApplication();
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.disableAudio = true;
        config.getTouchEventsForLiveWallpaper = true;
        config.useAccelerometer = true;

        ApplicationListener listener = new MainRenderer(getApplicationContext());
        initialize(listener, config);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    public void offsetChange(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
        Timber.i("offsetChange(xOffset:" + xOffset + " yOffset:" + yOffset + " xOffsetSteep:" + xOffsetStep + " yOffsetStep:" + yOffsetStep + " xPixelOffset:" + xPixelOffset + " yPixelOffset:" + yPixelOffset + ")");
    }

    @Override
    public void previewStateChange(boolean isPreview) {
        Timber.i("previewStateChange(isPreview:" + isPreview + ")");
    }

    @Override
    public void iconDropped(int x, int y) {
        Timber.i("iconDropped (" + x + ", " + y + ")");
    }
}
