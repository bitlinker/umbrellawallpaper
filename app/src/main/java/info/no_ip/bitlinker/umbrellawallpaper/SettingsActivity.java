package info.no_ip.bitlinker.umbrellawallpaper;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

/**
 * Wallpaper settings class
 *
 * Created by Bitlinker on 12/5/2017.
 */

public class SettingsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }
    }

    public static boolean getShowLogo(Context context) {
        return getPrefs(context).getBoolean("chkText", true);
    }

    public static float getRotationSpeed(Context context) {
        try {
            return Float.parseFloat(getPrefs(context).getString("edtRotationSpeed", ""));
        } catch (NumberFormatException e) {
            return 1.5F;
        }
    }

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
