package info.no_ip.bitlinker.umbrellawallpaper;

import android.content.Context;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import timber.log.Timber;

/**
 * Main wallpaper render class
 *
 * Created by Bitlinker on 12/5/2017.
 */

@SuppressWarnings("WeakerAccess")
public class MainRenderer implements ApplicationListener {
    private static final int VIEWPORT_MIN_SIDE = 480;
    private static final float SPRITE_SIZE = VIEWPORT_MIN_SIDE * 0.7F;

    private boolean mIsRunning;
    private final Context mContext;

    private float mTime = 0.F;

    private SpriteBatch mSpriteBatch;
    private OrthographicCamera mCamera;

    private Sprite mSprite;
    private Sprite mSpriteText;

    private boolean mIsShowLogo;
    private float mRotationSpeed;

    public MainRenderer(Context context) {
        mContext = context;
    }

    @Override
    public void create() {
        mSpriteBatch = new SpriteBatch();

        mSprite = new Sprite(new Texture("umbrella.png"));
        mSpriteText = new Sprite(new Texture("umbrella_text.png"));

        mCamera = new OrthographicCamera();
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        resume();
    }

    private void updateSpritePositions() {
        mSprite.setSize(SPRITE_SIZE, SPRITE_SIZE);
        mSprite.setOrigin(mSprite.getWidth() * 0.5F, mSprite.getHeight() * 0.5F);
        mSprite.setCenter(mSprite.getWidth() * 0.5F, mSprite.getHeight() * 0.5F);
        mSprite.setPosition(-mSprite.getWidth() * 0.5F, -mSprite.getHeight() * 0.5F);
        if (mIsShowLogo) {
            mSprite.translate(0.F, VIEWPORT_MIN_SIDE * 0.10F);
        }

        mSpriteText.setPosition(-mSpriteText.getWidth() * 0.5F, -mSpriteText.getHeight() * 0.5F);
        mSpriteText.setScale(0.7F);
        mSpriteText.translate(0.F, -VIEWPORT_MIN_SIDE * 0.35F);
    }

    @Override
    public void resize(int width, int height) {
        Timber.d("Resize: " + width + "x" + height);

        int vpWidth;
        int vpHeight;

        if (width > height) {
            vpHeight = VIEWPORT_MIN_SIDE;
            vpWidth = VIEWPORT_MIN_SIDE * width / height;
        } else {
            vpWidth = VIEWPORT_MIN_SIDE;
            vpHeight = vpWidth * height / width;
        }

        mCamera.setToOrtho(false, vpWidth, vpHeight);
        mCamera.position.set(0.F, 0.F, 0.F);
        mCamera.update();
    }

    @Override
    public void render() {
        if (!mIsRunning) return;

        mTime += Gdx.graphics.getDeltaTime();
        mSprite.setScale((float) Math.sin(mTime * mRotationSpeed), 1.F);

        Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.F);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mSpriteBatch.setProjectionMatrix(mCamera.combined);

        mSpriteBatch.begin();
        mSprite.draw(mSpriteBatch);
        if (mIsShowLogo) {
            mSpriteText.draw(mSpriteBatch);
        }
        mSpriteBatch.end();
    }

    @Override
    public void pause() {
        mIsRunning = false;
    }

    @Override
    public void resume() {
        mIsRunning = true;
        mIsShowLogo = SettingsActivity.getShowLogo(mContext);
        mRotationSpeed = SettingsActivity.getRotationSpeed(mContext);
        updateSpritePositions();
    }

    @Override
    public void dispose() {
        mSpriteBatch.dispose();
        mSprite.getTexture().dispose();
        mSpriteText.getTexture().dispose();
    }
}

